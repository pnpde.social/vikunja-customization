<?
    // -------------------------------------------------------------------------
    // CONFIG
    // -------------------------------------------------------------------------
    
    // create a file /etc/vikunja/webhook.config.php with the following content:
    
    // $vikunja_base = <base url of your vikunja installation>;
    // $webhook = <url of a discord webhook>;
    // $color['create'] = <decimal coded color>;
    // $color['delete'] = <decimal coded color>;
    // $color['change'] = <decimal coded color>;
    
    // https://www.spycolor.com/ converts hex colors to dec colors

    require_once("/etc/vikunja/webhook.config.php");
    
    

    // -------------------------------------------------------------------------
    // MAIN
    // -------------------------------------------------------------------------
    
    // read payload
    $raw = file_get_contents('php://input');

    if($raw) {
        $data = json_decode($raw);
        
        if($data) {
            // save payload data for developement; delete later
            file_put_contents('../logs/log.'.$data->event_name, json_encode($data, JSON_PRETTY_PRINT));
            
            // content is the first line of the discord message. set it to a description of what happend; use emojis
            switch($data->event_name) {
                case 'project.deleted':
                    $content = ':dividers:  Projekt gelöscht';
                    $color = $color['delete'];
                    break;
                case 'project.shared.team':
                    $content = ':people_with_bunny_ears_partying:  Projekt mit Team geteilt';
                    $color = $color['change'];
                    break;
                case 'project.shared.user':
                    $content = ':people_with_bunny_ears_partying:  Projekt mit Person geteilt';
                    $color = $color['change'];
                    break;
                case 'project.updated':
                    $content = ':dividers:  Projekt aktualisiert';
                    $color = $color['change'];
                    break;
                case 'task.assignee.created':
                    $content = ':construction_worker:  Person zugewiesen';
                    if(isset($data->data->assignee->name)) {
                        $content .= ': '.$data->data->assignee->name;
                    }
                    $color = $color['create'];
                    break;
                case 'task.assignee.deleted':
                    $content = ':construction_worker:  Zuweisung entfernt';
                    $color = $color['delete'];
                    break;
                case 'task.attachment.created':
                    $content = ':paperclip:  Neuer Anhang';
                    $color = $color['create'];
                    break;
                case 'task.attachment.deleted':
                    $content = ':paperclip:  Anhang gelöscht';
                    $color = $color['delete'];
                    break;
                case 'task.comment.created':
                    $content = ':speech_balloon:  Neuer Kommentar';
                    $color = $color['create'];
                    break;
                case 'task.comment.deleted':
                    $content = ':speech_balloon:  Kommentar gelöscht';
                    $color = $color['delete'];
                    break;
                case 'task.comment.edited':
                    $content = ':speech_balloon:  Kommentar bearbeitet';
                    $color = $color['change'];
                    break;
                case 'task.created':
                    $content = ':new:  Neuer Task';
                    $color = $color['create'];
                    break;
                case 'task.deleted':
                    $content = ':put_litter_in_its_place:  Task gelöscht';
                    $color = $color['delete'];
                    break;
                case 'task.relation.created':
                    $content = ':new:  Neue Beziehung';
                    $color = $color['create'];
                    break;
                case 'task.relation.deleted':
                    $content = ':put_litter_in_its_place:  Beziehung gelöscht';
                    $color = $color['delete'];
                    break;
                case 'task.updated':
                    $content = ':pencil:  Task aktualisiert';
                    $color = $color['change'];
                    break;
                default:
                    // if vikunja defines new event.types
                    $color = 0;
                    $content = $data->event_name;
            }
            
            // put task, comment, assignee etc into separate embeds. kinda lazy, maybe pretify later
            $embeds = [];
            if(isset($data->data->task)) {
                if(isset($data->data->task->done) && $data->data->task->done) {
                    $done_marker = '  :white_check_mark: ';
                } else {
                    $done_marker = '';
                }
                
                $fields = [];
                foreach($data->data->task->labels as $label) {
                    $fields[] = [
                        "name" => $label->title,
                        "value" => "",
                        "inline" => true
                    ];
                }
                
                $embeds[] = [
                    "author" => [
                        "name" => $data->data->doer->name,
                    ],
                    "title" => $data->data->task->title.$done_marker,
                    "url" => $vikunja_base."/tasks/".$data->data->task->id,
                    "description" => strip_tags($data->data->task->description),
                    "fields" => $fields,
                    "color" => $color,
                ];
            }
            if(isset($data->data->comment)) {
                $embeds[] = [
                    "author" => [
                        "name" => $data->data->comment->name,
                    ],
                    "title" => "Kommentar",
                    "description" => strip_tags($data->data->comment->comment),
                    "color" => $color,
                ];
            }
            if(isset($data->data->assignee) && $data->data->assignee->name) {
                $embeds[] = [
                    "title" => $data->data->assignee->name,
                    "color" => $color,
                ];
            }
            if(isset($data->data->attachment)) {
                $embeds[] = [
                    "author" => [
                        "name" => $data->data->created_by->name,
                    ],
                    "title" => "Anhang",
                    "description" => $data->data->attachment->file->name,
                    "color" => $color,
                ];
            }
            if(isset($data->data->relation)) {
                $other_task_id = $data->data->relation->other_task_id;
                foreach($data->data->task->related_tasks->related as $related) {
                    if($related->id == $other_task_id) {
                        $related_title = $related->title;
                        $related_description = $related->description;
                    }
                }
                
                $embeds[] = [
                    "author" => [
                        "name" => $data->data->relation->created_by->name,
                    ],
                    "title" => $related_title,
                    "description" => $related_description,
                    "color" => $color,
                ];
            }
            
            // construct the payload
            $payload = [ 
                "username" => "Vikunja",
                "content" => $content,
                "embeds" => $embeds
            ];

            // send it to our discord webhook
            discord($webhook, $payload);
        }
    } else {
        print "it's not crashing, that's something";
    }
    
    function discord($webhook, $payload) {
        $ch = curl_init ( $webhook );
        curl_setopt_array ( $ch, array (
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array (
                'payload_json' => json_encode($payload)
            ) 
        ));
        return curl_exec ( $ch );
    }
?>
